#!/bin/bash

master=$1

[ -n "$OAR_NODE_FILE" ] && nodes=($(uniq $OAR_NODE_FILE | sort -r))

if [ -z "$nodes" ] || [ ${#nodes[@]} -lt 2 ]; then
	echo "{}"
	exit 0
fi

FILENAME=deployment_generated.ini
echo "[calvin]" > $FILENAME
echo "${nodes[1]} cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"4g\" mode=\"local\" calvin_attr='{\"indexed_public\":  {\"node_name\": {\"group\": \"control\"}}, \"public\": {\"cost_cpu\": 1000, \"cost_ram\": 1000}}' control_port=5001 calvin_port=5004 netdata_port=20000 blackbox_port=9115" >> $FILENAME
#echo "${nodes[2]} cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"cloud\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"10000\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.2, \"cost_ram\": 0.2}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
x=0
for host in ${nodes[@]:2}; do
	if (( x % 3 == 0 )); then
		echo "$host cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"fog\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"10000\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.1, \"cost_ram\": 0.1}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
	elif (( x % 3 == 1 )); then
		echo "$host cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"60000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"fog\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"6000\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.1, \"cost_ram\": 0.1}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
	else
		echo "$host cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"30000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"fog\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"3000\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.1, \"cost_ram\": 0.1}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
	fi
	x=$((x+1))
done

ssh $master << EOF
sudo rm /tmp/csruntimes_id.txt /tmp/g5k_csruntimes_id.txt /tmp/fit_csruntimes_id.txt
cd ~/fitor
`~/fitor/env_variable.py`
echo "Stop calvin runtimes in G5K"
ansible-playbook -i $FILENAME calvin_g5k_single.yml --tags stop_runtimes
EOF
echo "Re-initializing runtimes in A8 nodes"
FIT_USER=$(head -n1 .iotlab)
ssh $FIT_USER@grenoble.iot-lab.info "cd fitor/iotlab/; python runtimes_exec.py -c calvin_runtimes_generated.json"
echo "Re-initializing runtimes in G5K"
ssh $master << EOF
cd ~/fitor/
`~/fitor/env_variable.py`
ansible-playbook -i $FILENAME calvin_g5k_single.yml
EOF
