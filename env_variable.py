#!/usr/bin/python
import requests
import os
import paramiko

IOTLAB_USER_FILE=os.path.expanduser("~/fitor/.iotlab")

if __name__ == "__main__":

    if not os.path.exists(IOTLAB_USER_FILE):
        print("FIT/IoT-LAB user file doesn't exist. Please create it: %s" % (IOTLAB_USER_FILE))
        sys.exit()

    fit_username = ""
    fit_pwd = ""
    with open(IOTLAB_USER_FILE, 'r') as fit_file:
        fit_username = fit_file.readline().rstrip()
        fit_pwd = fit_file.readline().rstrip()

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect("grenoble.iot-lab.info", username=fit_username)

    _, stdout, _ = ssh.exec_command("iotlab-experiment wait 2>&1 | egrep -o \"[0-9]+\"")
    stdout.channel.recv_exit_status()
    expid = stdout.read().strip("\n")
    BASE_ADDR='http://www.iot-lab.info/rest'
    addr = "/experiments/%s?resources" % expid
    r = requests.get(BASE_ADDR + addr, auth=(fit_username, fit_pwd))
    a8_nodes = ["node-" + server['network_address'] for server in r.json()['items'] if server['archi'] == "a8:at86rf231"]
    vpn_ips = []
    for server in a8_nodes:
        vpn_ips.append(os.popen('ssh %s@grenoble.iot-lab.info "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@%s \"/sbin/ifconfig tun0 | egrep -o \"inet addr:[0-9]+.[0-9]+.[0-9]+.[0-9]+\" | cut -d: -f2 | awk \"{ print $1}\"\""' % (fit_username,server)).read().split('\n')[0])
    print "export CALVIN_CONSTRAINED=\"%s\""% " ".join(str(x) for x in vpn_ips)
    m3_nodes = [server for server in r.json()['items'] if server['archi'] == "m3:at86rf231"]

    print "export SENSOR_SERVER_IP=\"2001:660:5307:310b::%s\"" % m3_nodes[0]['uid']

