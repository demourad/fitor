---
# tasks file for csruntime

- name: Copy calvin image
  copy:
    src: "~/fitor/docker/calvin:{{ calvin_version }}.tar"
    dest: "/tmp/calvin:{{ calvin_version }}.tar"
    mode: 0777
  ignore_errors: True
  tags:
    - restart_calvin

- name: Build/Load docker image if necessary
  block:
    - name: Load calvin
      become: yes
      docker_image:
        state: present
        name: "donassolo/calvin:{{ calvin_version }}"
        load_path: "/tmp/calvin:{{ calvin_version }}.tar"
  rescue:
    - name: Build calvin
      docker_image:
        state: present
        name: "donassolo/calvin:{{ calvin_version }}"
        archive_path: "/tmp/calvin:{{ calvin_version }}.tar"
        path: /g5k/
        buildargs:
          branch: "{{ calvin_version }}"
      become: yes
    - name: Set calvin built
      set_fact:
        calvinBuilt: true
  tags:
    - restart_calvin

- name: fetch archived calvin image
  fetch:
    src: "/tmp/calvin:{{ calvin_version }}.tar"
    dest: "./docker/calvin:{{ calvin_version }}.tar"
    flat: true
  when: calvinBuilt is defined
  ignore_errors: True
  tags:
    - restart_calvin

- name: Set CPU affinity variable
  set_fact:
    cpuAff: "--cpuset-cpus={{ cpuSet }}"
  when: cpuSet is defined
  tags:
    - restart_calvin

- name: Set RAM affinity variable
  set_fact:
    memAff: "--cpuset-mems={{ memSet }}"
  when: memSet is defined
  tags:
    - restart_calvin

- name: Set CPU Period and quota variable
  set_fact:
    cpuShares: "--cpu-period={{ cpuPeriod }} --cpu-quota={{ cpuQuota }}"
  when: cpuPeriod is defined and cpuQuota is defined
  tags:
    - restart_calvin

- name: Set Calvin attributes
  set_fact:
    attributes: "{{ calvin_attr | to_json }}"
  when: calvin_attr is defined
  tags:
    - restart_calvin

- name: Set RAM size variable
  set_fact:
    memLimit: "--memory={{ memSize }}"
  when: memSize is defined
  tags:
    - restart_calvin

- name: Check UUID file exists
  stat:
    path: "{{ csruntime_id_file }}"
  register: csruntime_id_file_stat
  become: yes
  tags:
    - restart_calvin

- name: Set empty UUID variable
  set_fact:
    uuid: ""
  become: yes
  when: csruntime_id_file_stat.stat.exists == False
  tags:
    - restart_calvin

- name: Get remote UUID content
  shell: cat {{ csruntime_id_file }}
  register: csruntime_id_file_cat
  when: csruntime_id_file_stat.stat.exists == True
  become: yes
  tags:
    - restart_calvin

- name: Set UUID variable
  set_fact:
    uuid: "--uuid \"{{ csruntime_id_file_cat.stdout }}\""
  when: csruntime_id_file_stat.stat.exists == True
  become: yes
  tags:
    - restart_calvin

- name: Display Variable
  debug: msg="{{ cpuAff }} {{ memAff }} {{ cpuShares }} {{ memLimit }} {{ attributes }} {{ uuid }}"
  tags:
    - restart_calvin

- name: Get running calvin runtimes
  become: yes
  shell: docker ps -aq | grep -v -E $(docker ps -aq --filter='name=cadvisor') | paste -sd " " -
  register: old_runtime_id
  tags:
    - stop_runtimes
    - restart_calvin

- name: Old runtime
  debug: msg="{{ old_runtime_id.stdout }}"
  tags:
    - stop_runtimes
    - restart_calvin

- name: Stop old container
  command: docker rm -f {{ old_runtime_id.stdout }}
  become: yes
  when: old_runtime_id.stdout != ""
  tags:
    - stop_runtimes
    - restart_calvin

- name: Creates calvin docker image in DHT mode
  command: docker create --net=host -l calvin --privileged -e CALVIN_ATTRIBUTES='{{ attributes }}' -v /etc/localtime:/etc/localtime:ro -v /var/run/docker.sock:/var/run/docker.sock -e CALVIN_IP={{ ansible_default_ipv4.address }} -e CALVIN_GLOBAL_STORAGE_TYPE="\"{{ mode }}\"" -e CALVIN_UUID="{{ uuid }}" -e CALVIN_PORT={{ calvin_port }} -e CALVIN_CONTROL_PORT={{ control_port }} -e BLACKBOX_PORT={{ blackbox_port }} -e NETDATA_PORT={{ netdata_port }} -p {{ mtail_port }}:{{ mtail_port }} -p {{ netdata_port }}:{{ netdata_port }} -p {{ calvin_port }}:{{ calvin_port }} -p {{ control_port }}:{{ control_port }} -p {{ blackbox_port }}:{{ blackbox_port }} {{ cpuAff }} {{ memAff }} {{ cpuShares }} {{ memLimit }}  donassolo/calvin:{{ calvin_version }}
  register: docker_run_dht
  become: yes
  when: mode == "dht"
  tags:
    - restart_calvin

- name: Creates calvin docker image in local mode
  command: docker create --net=host -l calvin --privileged  -e CALVIN_ATTRIBUTES='{{ attributes }}' -v /etc/localtime:/etc/localtime:ro -v /var/run/docker.sock:/var/run/docker.sock -e CALVIN_IP={{ ansible_default_ipv4.address }} -e CALVIN_PORT={{ calvin_port }} -e CALVIN_CONTROL_PORT={{ control_port }} -e CALVIN_GLOBAL_STORAGE_TYPE="\"{{ mode }}\"" -e CALVIN_UUID="{{ uuid }}" -p {{ netdata_port }}:{{ netdata_port }} -e BLACKBOX_PORT={{ blackbox_port }} -e NETDATA_PORT={{ netdata_port }} -p {{ mtail_port }}:{{ mtail_port }} -p {{ blackbox_port }}:{{ blackbox_port }} -p {{ calvin_port }}:{{ calvin_port }} -p {{ control_port }}:{{ control_port }} {{ cpuAff }} {{ memAff }} {{ cpuShares }} {{ memLimit }} donassolo/calvin:{{ calvin_version }}
  register: docker_run_local
  when: mode == "local"
  become: yes
  tags:
    - restart_calvin

- name: Creates calvin docker image in proxy mode
  command: docker create --net=host -l calvin --privileged  -e CALVIN_ATTRIBUTES='{{ attributes }}' -v /etc/localtime:/etc/localtime:ro -v /var/run/docker.sock:/var/run/docker.sock -e CALVIN_IP={{ ansible_default_ipv4.address }} -e CALVIN_PORT={{ calvin_port }} -e CALVIN_CONTROL_PORT={{ control_port }} -e CALVIN_GLOBAL_STORAGE_TYPE="\"{{ mode }}\"" -e CALVIN_UUID="{{ uuid }}" -e CALVIN_GLOBAL_STORAGE_PROXY="\"calvinip://{{ proxy_ip }}\"" -e BLACKBOX_PORT={{ blackbox_port }} -e NETDATA_PORT={{ netdata_port }} -p {{ mtail_port }}:{{ mtail_port }} -p {{ netdata_port }}:{{ netdata_port }} -p {{ blackbox_port }}:{{ blackbox_port }} -p {{ calvin_port }}:{{ calvin_port }} -p {{ control_port }}:{{ control_port }} {{ cpuAff }} {{ memAff }} {{ cpuShares }} {{ memLimit }} donassolo/calvin:{{ calvin_version }}
  register: docker_run_proxy
  when: mode == "proxy"
  become: yes
  tags:
    - restart_calvin

- name:
  set_fact: docker_id={{ docker_run_dht.stdout if mode == "dht" else docker_run_local.stdout if mode == "local" else docker_run_proxy.stdout }}
  tags:
    - restart_calvin

- name: Create temp file for calvin.conf
  tempfile:
    state: file
  register: calvin_config_file
  tags:
    - restart_calvin

- name: Set calvin config file
  copy:
    content: "{ \"global\" : { \"deployment_algorithm\" : \"{{ deployment_algorithm }}\", \"grasp\" : \"{{ grasp }}\", \"deployment_n_samples\" : {{ deployment_n_samples }}, \"maintenance_delay\" : {{ maintenance_interval }}, \"migration_cooldown\" : {{ migration_cooldown }}, \"reconfig_algorithm\" : \"{{ reconfig_algorithm }}\" }, \"calvinsys\" : { \"capabilities\" : { \"io.stdout\" : { \"module\" : \"ui.StandardOut\", \"attributes\": { \"ui_def\" : { \"control\" : { \"type\" : \"console\" }}} } } }, \"learn\" : { {{ learn_param }} } }"
    dest: "{{ calvin_config_file.path }}"
  tags:
    - restart_calvin

- name: Copy calvin config file
  command: docker cp {{ calvin_config_file.path }} {{ docker_id }}:/root/.calvin.conf
  become: yes
  tags:
    - restart_calvin

- name: Copy calvin config file2
  command: docker cp {{ calvin_config_file.path }} {{ docker_id }}:/root/calvin.conf
  become: yes
  tags:
    - restart_calvin

- name: Copy mtail scripts
  copy:
    src: "mtail/"
    dest: "/mtail/"
    mode: 0666
  become: yes
  tags:
    - restart_calvin

- name: Copy calvin mtail script to docker
  command: docker cp /mtail/calvin.mtail {{ docker_id }}:/mtail/calvin.mtail
  become: yes
  tags:
    - restart_calvin

- name: Starting calvin docker container
  command: docker start {{ docker_id }}
  become: yes
  tags:
    - restart_calvin

- name: Display docker id
  debug: msg="{{ docker_id }}"
  tags:
    - restart_calvin

- name: Copy resource monitoring scripts
  copy:
    src: "./resource_monitoring/"
    dest: "/resource_monitoring/"
    mode: 0777
  become: yes

- name: Installs CPU/RAM monitoring script dependencies
  shell: pip install -r requirements.txt
  become: yes
  args:
    chdir: /resource_monitoring/

- name: Kill old monitoring script
  shell: pkill -9 -f monitor.py
  become: yes
  ignore_errors: True
  tags:
    - restart_calvin

- name: Kill old monitoring REST script
  shell: pkill -9 -f monitor_rest.py
  become: yes
  ignore_errors: True
  tags:
    - restart_calvin

- name: Runs CPU/RAM monitoring script
  shell: python monitor.py -c {{ docker_id }} > /tmp/monitor_{{docker_id}}.log
  become: yes
  async: 2592000               # 60*60*24*30 – 1 month
  poll: 0
  args:
    chdir: /resource_monitoring/
  tags:
    - restart_calvin

- name: Runs CPU/RAM monitoring REST script
  shell: python monitor_rest.py -c {{ docker_id }} > /tmp/monitor_rest_{{docker_id}}.log
  become: yes
  async: 2592000               # 60*60*24*30 – 1 month
  poll: 0
  args:
    chdir: /resource_monitoring/
  tags:
    - restart_calvin

- name: Get docker IP address
  shell: docker inspect -f '{{ '{{' }}.NetworkSettings.IPAddress {{ '}}' }}' {{ docker_id }}
  become: yes
  register: docker_inspect_ip

- name:
  set_fact: ip_addr={{ ansible_default_ipv4.address }}

  #- name:
  #  set_fact: ip_addr={{ ansible_default_ipv4.address if mode == "dht" else docker_inspect_ip.stdout }}

- name: Remove old files
  shell: rm {{ csruntime_id_file }}
  become: yes
  ignore_errors: True

- name: Recover csruntime ID
  uri:
    url: http://{{ ip_addr }}:{{ control_port }}/id
    return_content: yes
  register: csruntime_id
  become: yes
  retries: 10
  delay: 3
  until: csruntime_id | success

- name: Display csruntime ID
  debug: msg="{{ csruntime_id.json.id }}"

- name: Save csruntime ID in a file
  copy:
    content="{{ csruntime_id.json.id }}"
    dest="{{ csruntime_id_file }}"
  become: yes
