#!/bin/bash

if [ -z "$1" ]
then
    exit
fi

IP=`echo $CALVIN_CONSTRAINED | cut -d" " -f1`
HTTP=$1

echo $HTTP

for i in `ssh root@$IP "curl -s http://[$HTTP]" | grep -Po '(?<![[:alnum:]]|[[:alnum:]]:)(?:(?:[a-f0-9]{1,4}:){7}[a-f0-9]{1,4}|(?:[a-f0-9]{1,4}:){1,6}:(?:[a-f0-9]{1,4}:){0,5}[a-f0-9]{1,4})(?![[:alnum:]]:?)'`;
do
    ssh root@$IP "ping6 -c 1 $i" > /dev/null 2>&1
    if [[ $? -eq 0 ]];
    then
        echo $i
    fi
done
