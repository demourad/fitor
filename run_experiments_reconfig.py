#!/usr/bin/python
import pandas as pd
import argparse
import subprocess
import os, sys
import time
import json
import re
import paramiko
import uuid
import datetime

IOTLAB_USER_FILE=os.path.expanduser("~/fitor/.iotlab")
INTERVAL=60
PORT=5001

def initial_information(ip, port):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)
    print("Runtime ID:")
    _, output, _ = ssh.exec_command('cscontrol http://%s:%d id' % (ip, port))
    output.channel.recv_exit_status()
    output_str = output.read()
    control = json.loads(output_str.strip("\n"))
    print (output_str)
    print("Nodes list:")
    _, output, _ = ssh.exec_command('cscontrol http://%s:%d nodes list' % (ip, port))
    output.channel.recv_exit_status()
    output_str = output.read()
    print (output_str)
    nodes = json.loads(output_str.strip("\n"))
    nodes.append(control)
    print("Nodes information:")
    for node in nodes:
        print("Node name: %s" % node)
        _, output, _ = ssh.exec_command('cscontrol http://%s:%d nodes info %s' % (ip, port, node))
        output.channel.recv_exit_status()
        print (output.read())

    return (nodes)

def run_app(calvin_path, app, ip, port):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)

    print('Adding application: exp %s app %d time %s' % (app["Exp_id"], app["App_id"], str(datetime.datetime.now())))
    filename = "%s/app%d_exp%s.calvin" % (calvin_path, app["App_id"], app["Exp_id"])
    deployname = "%s/app%d_exp%s.deployjson" % (calvin_path, app["App_id"], app["Exp_id"])
    conffile = "%s/exp%s_app%d_actor%d.txt" % (calvin_path, app["Exp_id"], app["App_id"], app["Trigger_id"])
    cmd = "sudo docker ps -a | grep 'donassolo/calvin' | awk '{print $1}'"
    _, output, _ = ssh.exec_command(cmd)
    output.channel.recv_exit_status()
    docker_id = output.read().strip("\n")
    print('\t Files: %s Deploy: %s Conf: %s' % (filename, deployname, conffile))
    cmd = "sudo docker cp %s %s:/calvin-base/" % (conffile, docker_id)
    _, output, _ = ssh.exec_command(cmd)
    output.channel.recv_exit_status()
    output_str = output.read()
    docker_id = output_str.strip("\n")
    print(output_str)

    cmd = "time cscontrol http://%s:%d deploy %s --reqs %s" % (ip, port, filename, deployname)
    _, output, _ = ssh.exec_command(cmd)
    output.channel.recv_exit_status()
    print(output.read())


def prometheus_logs(orgfile, control, path):
    print("Saving prometheus database")
    cmd = 'ssh %s "curl -XPOST http://localhost:9090/api/v2/admin/tsdb/snapshot"' % control
    print (subprocess.check_output(cmd, shell=True))

    cmd = 'ssh %s "cd %s; cp -r /opt/prometheus/data/snapshots/* snapshot/"' % (control, path)
    print (subprocess.check_output(cmd, shell=True))
    print >> orgfile, '* PROMETHEUS: '
    cmd = 'find %s/snapshot' % path
    print_org_run_cmd(orgfile, cmd)


def print_org_calvin_finish_info(orgfile, ip, runtimes_urls):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)

    print >> orgfile, '* CALVIN: '
    print_org_calvin_information_apps(orgfile, ip, runtimes_urls)
    print_org_calvin_information_actor(orgfile, ip, runtimes_urls)
    print_org_calvin_information_nodes(orgfile, ip, runtimes_urls)

def print_org_calvin_information_apps(orgfile, ip, runtimes_urls):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)
    print >> orgfile, "** Application list:"
    for runtime, url in runtimes_urls.iteritems():
        print >> orgfile, "*** %s : %s" % (url, runtime)
        cmd = "cscontrol %s applications list" % (url)
        print >> orgfile, cmd
        _, output, _ = ssh.exec_command(cmd)
        output.channel.recv_exit_status()
        print_org_cmd_output(orgfile, output.read())

def print_org_calvin_information_actor(orgfile, ip, runtimes_urls):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)
    print >> orgfile, "** Actor list:"
    for runtime, url in runtimes_urls.iteritems():
        print >> orgfile, "*** %s : %s" % (url, runtime)
        cmd = "cscontrol %s actor list" % (url)
        print >> orgfile, cmd
        _, output, _ = ssh.exec_command(cmd)
        output.channel.recv_exit_status()
        print_org_cmd_output(orgfile, output.read())

def print_org_calvin_information_nodes(orgfile, ip, runtimes_urls):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)
    print >> orgfile, "** Nodes list:"
    for runtime, url in runtimes_urls.iteritems():
        print >> orgfile, "*** %s : %s" % (url, runtime)
        cmd = "cscontrol http://%s:%d nodes info %s" % (ip, PORT, runtime)
        print >> orgfile, cmd
        _, output, _ = ssh.exec_command(cmd)
        output.channel.recv_exit_status()
        print_org_cmd_output(orgfile, output.read())


def collect_logs(orgfile, path, exp_id, runtimes_ips):
    print("Collecting G5K logs")
    for runtime, ip in runtimes_ips["g5k"].iteritems():
        print("IP: %s" % (ip))
        cmd = 'ssh %s "sudo docker ps -a | grep \'donassolo/calvin\' | awk \'{print \$1}\'"' % ip
        docker_id = subprocess.check_output(cmd, shell=True).strip("\n")
        cmd = 'ssh %s "sudo docker cp %s:/calvin-base/calvin.log /tmp/calvin.log"' % (ip, docker_id)
        print (subprocess.check_output(cmd, shell=True))
        cmd = "scp %s:/tmp/calvin.log %s/exp_%s_%s_calvin_g5k.log" % (ip, path, exp_id, runtime)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))
        cmd = "scp %s:/tmp/monitor_%s*.log %s/exp_%s_%s_monitor_%s_g5k.log" % (ip, docker_id, path, exp_id, runtime, docker_id)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))
        cmd = "scp %s:/tmp/monitor_rest_%s*.log %s/exp_%s_%s_monitor_rest_%s_g5k.log" % (ip, docker_id, path, exp_id, runtime, docker_id)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))
        cmd = "scp %s:/tmp/network_script_output.txt %s/exp_%s_%s_network_script_output_g5k.log" % (ip, path, exp_id, runtime)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))

    print("Collecting FIT logs")
    for runtime, ip in runtimes_ips["fit"].iteritems():
        print ("IP: %s" % (ip))
        cmd = 'scp root@%s:/tmp/calvin.log %s/exp_%s_%s_calvin_fit.log' % (ip, path, exp_id, runtime)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))
        cmd = 'scp root@%s:/tmp/monitor_linux.txt %s/exp_%s_%s_monitor_linux_fit.log' % (ip, path, exp_id, runtime)
        print (cmd)
        print (subprocess.check_output(cmd, shell=True))

    print >> orgfile, "* CALVIN LOGS:"
    print >> orgfile, "** G5K:"
    for runtime, ip in runtimes_ips["g5k"].iteritems():
        print >> orgfile, '*** %s : %s' % (ip, runtime)
        cmd = "cat %s/exp_%s_%s_calvin_g5k.log" % (path, exp_id, runtime)
        print_org_run_cmd(orgfile, cmd)

    print >> orgfile, "** FIT:"
    for runtime, ip in runtimes_ips["fit"].iteritems():
        print >> orgfile, '*** %s : %s' % (ip, runtime)
        cmd = 'cat %s/exp_%s_%s_calvin_fit.log' % (path, exp_id, runtime)
        print_org_run_cmd(orgfile, cmd)

def get_url_ip_for_runtimes(runtimes, ip, port):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip)

    runtimes_urls = {}
    runtimes_ips = { "g5k" : {}, "fit" : {}}
    for runtime in runtimes:
        cmd = "cscontrol http://%s:%d nodes info %s" % (ip, port, runtime)
        _, output, _ = ssh.exec_command(cmd)
        output.channel.recv_exit_status()
        out_json = json.loads(output.read().strip("\n"))

        runtime_url = ""
        runtime_ip = ""
        try:
            runtime_url = out_json["control_uris"][0].strip("\n")
            runtimes_urls[runtime] = runtime_url
            runtime_ip =  re.findall( r'http://(.+):[0-9]{1,5}', runtime_url)[0]
            if (any("endpoint" in s for s in out_json["attributes"]["indexed_public"])):
                runtimes_ips["fit"][runtime] = runtime_ip
            else:
                runtimes_ips["g5k"][runtime] = runtime_ip
        except:
            print ("Error getting IP/URL addresses for runtime: %s" % runtime)
            pass

    return(runtimes_urls, runtimes_ips)

def initial_setup(exp_dir, exp_df):
    """ Initial setup for the experiment
    """

    date = str(datetime.date.today())
    exp_id = str(uuid.uuid4())

    exp_path = os.path.expanduser("~/experiments/" + date + "/" + exp_id + "/")
    print("Creating directory: %s" % exp_path)
    if not os.path.exists(exp_path):
        os.makedirs(exp_path)

    #add experiment id
    exp_df['Exp_id'] = pd.Series(exp_id, index=exp_df.index)
    exp_df.to_csv(exp_path + "generated_experiments_list.csv", index=False)

    for d in ["original_files", "calvin_files", "logs"]:
        if not os.path.exists(exp_path + d):
            os.mkdir(exp_path +d)

    print("Copying original files to experiment directory")
    dst_dir = exp_path + "original_files/"
    from shutil import copyfile
    for fn in ["experiments_list.csv", "wl_app_char.csv", "wl_churn.csv", "wl_heter.csv", "wl_load.csv", "ew_param.csv"]:
        print("\tFile %s" % fn)
        copyfile(exp_dir + fn, dst_dir + fn)

    sys.stdout = os.fdopen(sys.stdout.fileno(), 'wb', 0)
    sys.stderr = os.fdopen(sys.stderr.fileno(), 'wb', 0)

    tee = subprocess.Popen(["tee", "%s/stdout.txt" % exp_path], stdin=subprocess.PIPE)
    tee2 = subprocess.Popen(["tee", "%s/stderr.txt" % exp_path], stdin=subprocess.PIPE)
    os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
    os.dup2(tee2.stdin.fileno(), sys.stderr.fileno())

    return exp_id,exp_path

def print_org_header(orgfile, orgfilename, exp_id, fit_username):
    print >> orgfile, "#+TITLE: Calvin exp %s" % exp_id
    print >> orgfile, "#+DATE: %s" % str(datetime.datetime.now())
    print >> orgfile, "#+FILE: %s" % orgfilename
    print >> orgfile, '* RESERVATION INFO:'
    print >> orgfile, '** G5K:'
    cmd = "cat $OAR_NODE_FILE"
    print_org_run_cmd(orgfile, cmd)
    cmd = "cat $OAR_NODE_FILE | uniq | sort -r"
    print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '** FIT:'
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect("grenoble.iot-lab.info", username=fit_username)
    _, stdout, _ = ssh.exec_command("iotlab-experiment wait 2>&1 | egrep -o \"[0-9]+\"")
    stdout.channel.recv_exit_status()
    expid = stdout.read().strip("\n")
    cmd = 'ssh %s@grenoble.iot-lab.info "iotlab-experiment get -i %s -r"' % (fit_username, expid)
    print_org_run_cmd(orgfile, cmd)

def print_org_cmd_output(orgfile, cmdoutput):
    print >> orgfile, '#+BEGIN_EXAMPLE'
    print >> orgfile, cmdoutput
    print >> orgfile, '#+END_EXAMPLE'

def print_org_run_cmd(orgfile, cmd):
    print >> orgfile, cmd
    output = subprocess.check_output(cmd, shell=True)
    print_org_cmd_output(orgfile, output)

def print_org_machine_info(orgfile, runtimes_ips, master):
    print >> orgfile, '* MACHINE INFO:'
    print >> orgfile, '** G5K:'

    ip = master
    print >> orgfile, '*** %s : %s' % (ip, 'master')
    print >> orgfile, '**** HOSTNAME:'
    cmd = 'hostname'
    print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '**** CPU INFO:'
    cmd = 'cat /proc/cpuinfo'
    print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '**** MEM INFO:'
    cmd = 'cat /proc/meminfo'
    print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '**** LINUX VERSION:'
    cmd = 'uname -a'
    print_org_run_cmd(orgfile, cmd)

    for runtime, ip in runtimes_ips["g5k"].iteritems():
        print >> orgfile, '*** %s : %s' % (ip, runtime)
        print >> orgfile, '**** HOSTNAME:'
        cmd = 'ssh %s "hostname"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** CPU INFO:'
        cmd = 'ssh %s "cat /proc/cpuinfo"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** MEM INFO:'
        cmd = 'ssh %s "cat /proc/meminfo"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** LINUX VERSION:'
        cmd = 'ssh %s "uname -a"' % ip
        print_org_run_cmd(orgfile, cmd)

    print >> orgfile, '** FIT:'
    for runtime, ip in runtimes_ips["fit"].iteritems():
        print >> orgfile, '*** %s : %s' % (ip, runtime)
        print >> orgfile, '**** HOSTNAME:'
        cmd = 'ssh root@%s "hostname"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** CPU INFO:'
        cmd = 'ssh root@%s "cat /proc/cpuinfo"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** MEM INFO:'
        cmd = 'ssh root@%s "cat /proc/meminfo"' % ip
        print_org_run_cmd(orgfile, cmd)
        print >> orgfile, '**** LINUX VERSION:'
        cmd = 'ssh root@%s "uname -a"' % ip
        print_org_run_cmd(orgfile, cmd)

def print_org_code_rev(orgfile, runtimes_ips, master):
    print >> orgfile, '* CODE REVS:'

    print >> orgfile, '** Phd repository'
    print >> orgfile, '*** G5K:'
    ip = master
    print >> orgfile, '**** %s : %s' % (ip, 'master')
    cmd = 'ssh %s "cd ~/fitor/; git log -1"' % ip
    print_org_run_cmd(orgfile, cmd)
    cmd = 'ssh %s "cd ~/fitor/; git status"' % ip
    print_org_run_cmd(orgfile, cmd)
    cmd = 'ssh %s "cd ~/fitor/; git --no-pager diff"' % ip
    print_org_run_cmd(orgfile, cmd)
    for runtime, ip in runtimes_ips["g5k"].iteritems():
        print >> orgfile, '**** %s : %s' % (ip, runtime)
        cmd = 'ssh %s "cd ~/fitor/; git log -1"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh %s "cd ~/fitor/; git status"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh %s "cd ~/fitor/; git --no-pager diff"' % ip
        print_org_run_cmd(orgfile, cmd)

    print >> orgfile, '** Calvin repository'
    print >> orgfile, '*** G5K:'
    for runtime, ip in runtimes_ips["g5k"].iteritems():
        print >> orgfile, '**** %s : %s' % (ip, runtime)
        cmd = 'ssh %s "cd /calvin-base/; git log -1"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh %s "cd /calvin-base/; git status"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh %s "cd /calvin-base/; git --no-pager diff"' % ip
        print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '*** FIT:'
    for runtime, ip in runtimes_ips["fit"].iteritems():
        print >> orgfile, '**** %s : %s' % (ip, runtime)
        cmd = 'ssh root@%s "cd /calvin-base/; git log -1"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh root@%s "cd /calvin-base/; git status"' % ip
        print_org_run_cmd(orgfile, cmd)
        cmd = 'ssh root@%s "cd /calvin-base/; git --no-pager diff"' % ip
        print_org_run_cmd(orgfile, cmd)

def print_org_stds(orgfile, path):

    print >> orgfile, '* STDOUT OUTPUT:'
    cmd = 'cat %s/stdout.txt' % path
    print_org_run_cmd(orgfile, cmd)
    print >> orgfile, '* STDERR OUTPUT:'
    cmd = 'cat %s/stderr.txt' % path
    print_org_run_cmd(orgfile, cmd)

def create_apps(exp_path, calvin_path, master):
    seed = "40"
    cmd = ["Rscript", "./reconfig_create_apps.R", "-e", exp_path + "/generated_experiments_list.csv", "-c", exp_path + "/original_files/wl_app_char.csv", "-x", exp_path + "/original_files/wl_churn.csv", "-t", exp_path + "/original_files/wl_heter.csv", "-l", exp_path + "/original_files/wl_load.csv", "-d", calvin_path, "-s", seed]
    print("Generating calvin files, cmd %s" % " ".join(str(i) for i in cmd))
    cmd_str = "ssh %s \"cd ~/fitor; %s\"" % (master, " ".join(str(i) for i in cmd))
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(master)
    _, output, _ = ssh.exec_command('cd ~/fitor/')
    output.channel.recv_exit_status()
    _, output, _ = ssh.exec_command(cmd_str)
    output.channel.recv_exit_status()
    print cmd_str
    #subprocess.call (cmd_str)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Runs a set of experiments')
    parser.add_argument('-d', '--directory', type=str, help='Folder containing experiment files', required=True)
    parser.add_argument('-c', '--control', type=str, help="IP address of Calvin's control node", required=True)
    parser.add_argument('-m', '--master', type=str, help="IP address of master node", required=True)
    args = parser.parse_args()

    exp_df = pd.read_csv(os.path.abspath(args.directory) + '/experiments_list.csv', na_filter=False)
    ew_df = pd.read_csv(os.path.abspath(args.directory) + '/ew_param.csv', na_filter=False)
    exp_df = exp_df.merge(ew_df, how="left")

    if len(exp_df) != 1:
        import sys
        print("Should be only one experiment per file")
        print(exp_df)
        sys.exit()

    if not os.path.exists(IOTLAB_USER_FILE):
        print("FIT/IoT-LAB user file doesn't exist. Please create it: %s" % (IOTLAB_USER_FILE))
        sys.exit()

    fit_username = ""
    fit_pwd = ""
    with open(IOTLAB_USER_FILE, 'r') as fit_file:
        fit_username = fit_file.readline().rstrip()
        fit_pwd = fit_file.readline().rstrip()

    exp_id, exp_path = initial_setup(args.directory, exp_df)
    orgfilename = "output_exp_%s.org" % exp_id
    orgfile = open(exp_path + orgfilename, 'w+')
    print_org_header(orgfile, orgfilename, exp_id, fit_username)
    calvin_path = exp_path + "/calvin_files/"
    logs_path = exp_path + "/logs/"

    create_apps(exp_path, calvin_path, args.master)

    app_df = pd.read_csv(calvin_path + 'app_list.csv')

#    import sys
#    sys.exit()

    print('-----------------------------------------------------------------')
    for idx, exp in exp_df.iterrows():
        exp.fillna({"Calvin_redeploy": 'NA'}, inplace=True)
        print('Re-initializing runtimes')
        if exp['Calvin_redeploy'].startswith('app_learn'):
            print (subprocess.check_output("./runtimes_reconfig.sh -b %s -r %s -d %s -m %s -c %s -n %s -l '\"epsilon\": %f, \"K\": %d, \"f_max\": %f, \"lambda\": %f, \"learn_rate\": %f, \"alpha\": %f, \"beta\": %f'" % (exp['Calvin_version'], exp['Calvin_redeploy'], exp['Calvin_deploy'], exp['Calvin_maint_interval'], exp['Calvin_mig_cooldown'], args.master, exp['epsilon'], exp['K'], exp['f_max'], exp['lambda'], exp['learn_rate'], exp['alpha'], exp['beta']), shell=True))
        else:
            print (subprocess.check_output("./runtimes_reconfig.sh -b %s -r %s -d %s -m %s -c %s -n %s" % (exp['Calvin_version'], exp['Calvin_redeploy'], exp['Calvin_deploy'], exp['Calvin_maint_interval'], exp['Calvin_mig_cooldown'], args.master), shell=True))
        runtimes = initial_information(args.control, PORT)
        print("Running experiment %s" % exp_id)
        for app_idx, app in app_df.loc[app_df['Exp_id'] == exp_id].iterrows():
            if exp['Calvin_deploy'] == "fixed":
                cmd = 'curl -X POST  -d \'{"value": "true" }\' http://%s:%d/storage/batch' % (args.control, PORT)
                print (subprocess.check_output(cmd, shell=True))
            time.sleep(INTERVAL)
            run_app(calvin_path, app, args.control, PORT)
            if exp['Calvin_deploy'] == "fixed":
                cmd = 'curl -X POST  -d \'{"value": "false" }\' http://%s:%d/storage/batch' % (args.control, PORT)
                print (subprocess.check_output(cmd, shell=True))
        print('-----------------------------------------------------------------')
        print('Finished adding applications, time: %s, sleeping for: %d' % (str(datetime.datetime.now()), exp['Exp_duration']))
        time.sleep(exp['Exp_duration'])
        break

    runtimes_urls, runtimes_ips = get_url_ip_for_runtimes(runtimes, args.control, PORT)
    print_org_calvin_finish_info(orgfile, args.control, runtimes_urls)
    print_org_machine_info(orgfile, runtimes_ips, args.master)
    print_org_code_rev(orgfile, runtimes_ips, args.master)
    collect_logs(orgfile, logs_path, exp_id, runtimes_ips)
    print_org_stds(orgfile, exp_path)
    prometheus_logs(orgfile, args.master, exp_path)
    subprocess.call(["tar", "-czf", exp_path + "logs.tar.gz", "-C", exp_path, "logs"])
    subprocess.call(["rm", "-rf", logs_path])
