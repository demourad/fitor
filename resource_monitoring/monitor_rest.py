import docker
import argparse
import json
import socket
from flask import Flask
from monitor import get_cpu_usage, get_ram_usage, update_cpu, update_memory
app = Flask(__name__)

container_id=""
ip_addr=None

@app.route('/node/resource')
def read_resources():
    data = client.stats(container_id, stream=False)
    cpu = get_cpu_usage(data)
    ram = get_ram_usage(data)
    update_cpu(container_id, data, ip_addr)
    update_memory(container_id, data, ip_addr)
    response = {'cpu': cpu, 'ram': ram}
    return json.dumps(response), 200
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='REST interface to read docker stats. It is only valid for containers running a single csruntime and so will use only 1 core.')
    parser.add_argument('-c', '--container', type=str, help='Container ID to monitor')
    args = parser.parse_args()

    if not args.container:
        parser.print_usage()
        parser.exit()

    container_id = args.container
    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    ip_addr = client.inspect_container(args.container)['NetworkSettings']['IPAddress']
    if not ip_addr:
        ip_addr = socket.gethostbyname(socket.gethostname())
    app.run(host='0.0.0.0', port=6000)
