from __future__ import division
import argparse
import docker
import time
import requests
import socket

SLEEP=60 #monitor information each sleep seconds


def get_cpu_usage(stats):
    pre_usage = float(stats['precpu_stats']['cpu_usage']['total_usage'])
    cpu_usage = float(stats['cpu_stats']['cpu_usage']['total_usage'])
    throttled =  float(stats['cpu_stats']['throttling_data']['throttled_time'])
    pre_thro =  float(stats['precpu_stats']['throttling_data']['throttled_time'])
    system_usage = float(stats['cpu_stats']['system_cpu_usage'])
    pre_system_usage = float(stats['precpu_stats']['system_cpu_usage'])

    cores = 0
    try:
        cores = stats['cpu_stats']['online_cpus']
    except:
        pass

    if cores == 0:
        cores = len(stats['cpu_stats']['cpu_usage']['percpu_usage'])

    diff_usage = cpu_usage - pre_usage
    diff_throttled = throttled - pre_thro
    diff_system = system_usage - pre_system_usage

    if diff_system <= 0 or diff_usage < 0:
        return -1

    percent = ((diff_usage + diff_throttled)/diff_system)*cores*100
    return percent

def update_cpu(container, stats, ip_addr, port = 5001):

    percent = get_cpu_usage(stats)
    if percent == -1:
        print "Invalid delta, system: %d, usage: %d" % (diff_system, diff_usage)
        return

    addr = 'http://%s:%d/node/resource/cpuAvail' % (ip_addr, port)
    perc_rounded = 100 - percent
    data = '{"value": %d}' % perc_rounded
    r = requests.post(addr, data)

    print "Container: %s. Available CPU percentage %d" % (container, perc_rounded)

    if not r.ok:
        print "Error updating available CPU in csruntime. Reason: " + r.reason
        print addr + " " + data


def get_ram_usage(stats):
    usage = stats['memory_stats']['usage']
    limit = stats['memory_stats']['limit']
    percent = (usage/limit)*100
    return percent

def update_memory(container, stats, ip_addr, port = 5001):
    """
    Update memory available in runtime runnint at container
    container: name or ID of container
    stats: stats read from container
    ip_addr: IP address to access runtime's control API
    port: control port
    """

    percent = get_ram_usage(stats)
    print 'Container: %s, percentage usage: %f' % (container, percent)
    
    addr = 'http://%s:%d/node/resource/memAvail' % (ip_addr, port)
    perc_rounded = 100 - percent
    data = '{"value": %d}' % perc_rounded
    r = requests.post(addr, data)

    if not r.ok:
        print "Error updating available memory in csruntime. Reason: " + r.reason
        print addr + " " + data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reads docker stats and updates CPU/RAM of Calvin runtimes. It is only valid for containers running a single csruntime and so will use only 1 core.')
    parser.add_argument('-c', '--container', type=str, help='Container ID to monitor')
    args = parser.parse_args()

    if not args.container:
        parser.print_usage()
        parser.exit()

    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    ip_addr = client.inspect_container(args.container)['NetworkSettings']['IPAddress']
    if not ip_addr:
        ip_addr = socket.gethostbyname(socket.gethostname())

    print "Container IP address: %s" % ip_addr
    data = client.stats(args.container, decode=True, stream=False)
    update_memory(args.container, data, ip_addr)
    update_cpu(args.container, data, ip_addr)
    while True:
        data = client.stats(args.container, decode=True, stream=False)
        update_memory(args.container, data, ip_addr)
        update_cpu(args.container, data, ip_addr)
        time.sleep(SLEEP)
