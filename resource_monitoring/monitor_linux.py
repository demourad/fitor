import requests
import psutil
import netifaces as ni

INTERFACE='tun0'
SLEEP=60

def update_cpu(ip_addr, port = 5001):
    percent = psutil.cpu_percent(interval=SLEEP)
    addr = 'http://%s:%d/node/resource/cpuAvail' % (ip_addr, port)
    perc_rounded = 100 - percent
    data = '{"value": %d}' % perc_rounded
    try:
        r = requests.post(addr, data, timeout=60)
        if not r.ok:
            print "Error updating available CPU in csruntime. Reason: " + r.reason
            print addr + " " + data
    except:
        print "Error connecting to csruntime"
        pass

    print "Available CPU percentage %d" % (perc_rounded)



def update_memory(ip_addr, port = 5001):
    percent = psutil.virtual_memory().percent
    addr = 'http://%s:%d/node/resource/memAvail' % (ip_addr, port)
    perc_rounded = 100 - percent
    data = '{"value": %d}' % perc_rounded
    try:
        r = requests.post(addr, data, timeout=60)
        if not r.ok:
            print "Error updating available memory in csruntime. Reason: " + r.reason
            print addr + " " + data
    except:
        print "Error connecting to csruntime"
        pass

    print 'RAM percentage usage: %f' % (percent)


if __name__ == "__main__":
    ip_addr = ni.ifaddresses('tun0')[ni.AF_INET][0]['addr']

    print "Calvin's IP address: %s" % ip_addr
    while True:
        try:
            update_memory(ip_addr)
            update_cpu(ip_addr)
        except:
            print "Generic error"
            pass
