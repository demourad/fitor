#!/usr/bin/python
import requests
import os, sys
import argparse
import paramiko
import json
import time

IOTLAB_USER_FILE=os.path.expanduser("~/fitor/.iotlab")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MAGIC')
    parser.add_argument('-c', '--control', type=str, help='Address of control node', required=True)
    parser.add_argument('-m', '--master', type=str, help='Address of master node', required=True)
    parser.add_argument('--cpuA8', type=int, help='CPU power of A8 nodes', default=1000)
    if not os.path.exists(IOTLAB_USER_FILE):
        print("FIT/IoT-LAB user file doesn't exist. Please create it: %s" % (IOTLAB_USER_FILE))
        sys.exit()

    fit_username = ""
    fit_pwd = ""
    with open(IOTLAB_USER_FILE, 'r') as fit_file:
        fit_username = fit_file.readline().rstrip()
        fit_pwd = fit_file.readline().rstrip()

    args = parser.parse_args()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect("grenoble.iot-lab.info", username=fit_username)

    _, stdout, _ = ssh.exec_command("iotlab-experiment wait 2>&1 | egrep -o \"[0-9]+\"")
    stdout.channel.recv_exit_status()
    expid = stdout.read().strip("\n")

    print("Waiting for A8 boot")
    _, stdout, _ = ssh.exec_command("iotlab-ssh wait-for-boot")
    stdout.channel.recv_exit_status()

    cmd = "cd fitor/iotlab; python setup_iotlab.py -i %s -m %s --cpuA8 %s" % (expid, args.control, args.cpuA8)
    print("Initializing iotlab ID: %s, cmd %s" % (expid, cmd))
    _, stdout, _ = ssh.exec_command(cmd, get_pty=True)
    for line in iter(lambda: stdout.readline(2048), ""):
        print(line.encode('utf-8'),)
    stdout.channel.recv_exit_status()
    print("Finished setup iotlab")

    BASE_ADDR='http://www.iot-lab.info/rest'
    addr = "/experiments/%s?resources" % expid
    r = requests.get(BASE_ADDR + addr, auth=(fit_username, fit_pwd))
    a8_nodes = ["node-" + server['network_address'] for server in r.json()['items'] if server['archi'] == "a8:at86rf231"]

    _, stdout, _ = ssh.exec_command("cat fitor/iotlab/calvin_runtimes_generated.json")
    fit_runtimes_json = json.loads(stdout.read())
    for server in a8_nodes:
        for retry in range(1, 5):
            try:
                vpn_ip = os.popen('ssh %s@grenoble.iot-lab.info "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@%s \"/sbin/ifconfig tun0 | egrep -o \"inet addr:[0-9]+.[0-9]+.[0-9]+.[0-9]+\" | cut -d: -f2 | awk \"{ print $1}\"\""' % (fit_username, server)).read().split('\n')[0]
                vpn_req = None
                vpn_req = requests.get("http://%s:5001/id" % vpn_ip, timeout=5)
                for node in fit_runtimes_json["nodes"]:
                    if node["addr"] == server:
                        node["uuid"] = vpn_req.json()["id"]
                break
            except:
                time.sleep(5)
                pass

    with open('calvin_runtimes_generated.json', 'w') as outfile:
        json.dump(fit_runtimes_json, outfile)
        outfile.close()

    sftp = ssh.open_sftp()
    sftp.put('calvin_runtimes_generated.json', 'fitor/iotlab/calvin_runtimes_generated.json')

    vpn_ips = []
    for server in a8_nodes:
        vpn_ips.append(os.popen('ssh %s@grenoble.iot-lab.info "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@%s \"/sbin/ifconfig tun0 | egrep -o \"inet addr:[0-9]+.[0-9]+.[0-9]+.[0-9]+\" | cut -d: -f2 | awk \"{ print $1}\"\""' % (fit_username, server)).read().split('\n')[0])
    os.environ["CALVIN_CONSTRAINED"] = " ".join(str(x) for x in vpn_ips)
    m3_nodes = [server for server in r.json()['items'] if server['archi'] == "m3:at86rf231"]

    os.environ["SENSOR_SERVER_IP"] = "2001:660:5307:310b::%s" % m3_nodes[0]['uid']
    os.system("ansible-playbook -i g5k-calvin.sh calvin_g5k.yml")
    print("First run of runtimes")
    os.system("./runtimes_g5k_fit_first.sh %s" % args.master)
    print("Ended")
