from setuptools import setup, find_packages

setup(
    install_requires=[
        'ansible==2.7.8',
        'requests',
        'paramiko',
        'pandas'
    ]
)
