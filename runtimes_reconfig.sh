#!/bin/bash

while getopts "b:r:d:m:c:n:l:" opt; do
	case $opt in
		b)
			branch=$OPTARG
			;;
		d)
			deploy=$OPTARG
			;;
		r)
			reconf=$OPTARG
			;;
		m)
			maint_interval=$OPTARG
			;;
		c)
			mig_cooldown=$OPTARG
			;;
		n)
			master=$OPTARG
			;;
		l)
			learn_param=$OPTARG
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			;;
	esac
done

FIT_USER=$(head -n1 .iotlab)

[ -n "$OAR_NODE_FILE" ] && nodes=($(uniq $OAR_NODE_FILE | sort -r))

if [ -z "$nodes" ] || [ ${#nodes[@]} -lt 2 ]; then
	echo "{}"
	exit 0
fi

FILENAME=deployment_generated.ini
echo "[calvin]" > $FILENAME
echo "${nodes[1]} cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"4g\" mode=\"local\" calvin_attr='{\"indexed_public\":  {\"node_name\": {\"group\": \"control\"}, \"cpuTotal\": \"1\", \"memTotal\":\"1K\"}, \"public\": {\"cost_cpu\": 1000, \"cost_ram\": 1000}}' control_port=5001 calvin_port=5004 netdata_port=20000 blackbox_port=9115" >> $FILENAME
#echo "${nodes[2]} cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"cloud\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"10000\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.2, \"cost_ram\": 0.2}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
x=1
for host in ${nodes[@]:2}; do
	cpu=$(ssh $host "sudo docker run donassolo/matrix matrix.py 1500")
	cpu=$(echo "$cpu/1000000" | bc -l)
	cpu=$(printf '%.0f\n' $cpu)
	echo "$host cpuSet=\"0\" memSet=\"0\" cpuPeriod=\"100000\" cpuQuota=\"100000\" memSize=\"2g\" mode=\"proxy\" proxy_ip=\"${nodes[1]}:5004\" calvin_attr='{\"indexed_public\": {\"node_name\": {\"group\": \"fog\", \"name\": \"${x}\"}, \"cpuAffinity\": \"dedicated\", \"cpuTotal\": \"$cpu\", \"memTotal\":\"1G\"}, \"public\": {\"cost_cpu\": 0.1, \"cost_ram\": 0.1}}' control_port=5001 calvin_port=5002 netdata_port=19999 blackbox_port=9115" >> $FILENAME
	x=$((x+1))
done
ssh $master << EOF
cd ~/fitor/
`~/fitor/env_variable.py`
echo "Stop calvin runtimes in G5K"
ansible-playbook -i $FILENAME calvin_g5k_single.yml --tags stop_runtimes
EOF
echo "Re-initializing runtimes in A8 nodes"
cmd="cd fitor/iotlab/; python -u runtimes_exec.py -c calvin_runtimes_generated.json"
if [ -n "$branch" ];
then
	cmd="${cmd} -b ${branch}"
fi
if [ -n "$deploy" ];
then
	cmd="${cmd} -d ${deploy}"
fi
if [ -n "$reconf" ];
then
	cmd="${cmd} -r ${reconf}"
fi
if [ -n "$maint_interval" ];
then
	cmd="${cmd} -m ${maint_interval}"
fi
if [ -n "$mig_cooldown" ];
then
	cmd="${cmd} -co ${mig_cooldown}"
fi
if [ -n "$learn_param" ];
then
       cmd="${cmd} -l '${learn_param}'"
fi

echo "Running cmd: ${cmd}"
ssh $FIT_USER@grenoble.iot-lab.info "${cmd}"
ssh $FIT_USER@grenoble.iot-lab.info "cd fitor/iotlab/; python -u runtimes_install_monitoring.py -c calvin_runtimes_generated.json"
echo "Re-initializing runtimes in G5K"
if [ -n "$branch" ];
then
	echo "Setting calvin version to: ${branch} in calvin_g5k_single.yml file"
	sed -i -E "s/calvin_version: \"[^\"]+\"/calvin_version: \"${branch}\"/" calvin_g5k_single.yml
fi
if [ -n "$deploy" ];
then
	echo "Setting deployment algorithm to: ${deploy} in calvin_g5k_single.yml file"
	sed -i -E "s/deployment_algorithm: \"[^\"]+\"/deployment_algorithm: \"${deploy}\"/" calvin_g5k_single.yml
fi
if [ -n "$reconf" ];
then
	echo "Setting reconfig algorithm to: ${reconf} in calvin_g5k_single.yml file"
	sed -i -E "s/reconfig_algorithm: \"[^\"]+\"/reconfig_algorithm: \"${reconf}\"/" calvin_g5k_single.yml
fi
if [ -n "$maint_interval" ];
then
	echo "Setting maintenance interval to: ${maint_interval} in calvin_g5k_single.yml file"
	sed -i -E "s/maintenance_interval: [0-9]+/maintenance_interval: ${maint_interval}/" calvin_g5k_single.yml
fi
if [ -n "$mig_cooldown" ];
then
	echo "Setting migration cooldown interval to: ${mig_cooldown} in calvin_g5k_single.yml file"
	sed -i -E "s/migration_cooldown: [0-9]+/migration_cooldown: ${mig_cooldown}/" calvin_g5k_single.yml
fi
if [ -n "$learn_param" ];
then
       echo "Setting learn params to: ${learn_param} in calvin_g5k_single.yml file"
       sed -i -E "s/learn_param: '.+'/learn_param: '${learn_param}'/" calvin_g5k_single.yml
fi

ssh $master << EOF
cd ~/fitor/
`~/fitor/env_variable.py`
ansible-playbook -i $FILENAME calvin_g5k_single.yml --tags restart_calvin
EOF
