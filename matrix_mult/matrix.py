#!/usr/bin/python

import random
import numpy as np
import time
import sys

random.seed(42)
N=int(sys.argv[1])
a = [ [random.random() for i in range(0,N)] for j in range(0,N)]
b = [ [random.random() for i in range(0,N)] for j in range(0,N)]
old = time.time()
np.matmul(a,b)
elapsed = time.time() - old
flops = N*N*N/elapsed
print flops
