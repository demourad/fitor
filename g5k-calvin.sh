#!/bin/bash

[ -n "$OAR_NODE_FILE" ] && nodes=($(uniq $OAR_NODE_FILE | sort -r))

#if [ -z "$nodes" ] || [ ${#nodes[@]} -lt 2 ]; then
if [ -z "$nodes" ] || [ ${#nodes[@]} -lt 2 ] || [ -z "$SENSOR_SERVER_IP" ] || [ -z "$CALVIN_CONSTRAINED" ]; then
    echo "{}"
    exit 0
fi

prometheus=${nodes[0]}

cat << EOF
{
    "prometheus" : {
        "hosts" : [ "$prometheus" ]
    },
    "calvin" : {
        "hosts" : [ $(echo ${nodes[@]:1} | perl -lane 'print join ", ",  map { "\"" . $_ . "\"" } @F ') ]
    },
    "calvin-constrained" : {
        "hosts" : [ $(echo $CALVIN_CONSTRAINED | perl -lane 'print join ", ",  map { "\"" . $_ . "\"" } @F ') ]
    },
    "sensors" : {
        "hosts" : [ $(./get_sensors.sh $SENSOR_SERVER_IP | tr '\n' ' ' | perl -lane 'print join ", ",  map { "\"" . $_ . "\"" } @F ') ]
    },
    "_meta" : {
        "hostvars": {
            $(echo ${nodes[@]:1} | perl -lane 'print join ",\n            ",  map { "\"" . $_ . "\" : { \"mode\" : \"dht\" } "} @F ')
        }
    }
}
EOF
