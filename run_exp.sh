kadeploy3 -f $OAR_NODE_FILE -e debian9-x64-std -k

sleep 1m
master=$(cat $OAR_NODE_FILE | uniq | sort -r | sed -n 1p)
server=$(cat $OAR_NODE_FILE | uniq | sort -r | sed -n 2p)
server_ip=$(dig +short $server)
echo "Master: $master Server: $server IP: $server_ip"
for i in $(cat $OAR_NODE_FILE | uniq); do
	echo $i
	ssh -o ServerAliveInterval=55 $i "while true; do ls; sleep 1m; done" > /dev/null 2>&1 &
done
cd ~/fitor/
echo "Initializing G5K and FIT platforms"
./initial_g5k_fit_setup.py -c ${server_ip}:5004 -m $master --cpuA8 13
echo "Initialization done... Start running experiments"

python run_experiments_reconfig.py -d exp_example/ -c ${server_ip} -m ${master}
