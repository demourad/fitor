import json
import argparse
import paramiko

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Installs monitoring script in A8 nodes.')
    parser.add_argument('-c', '--config', type=str, help='Configuration file', required=True)
    args = parser.parse_args()

    with open(args.config) as json_data_file:
        cfg = json.load(json_data_file)

    ssh_stdouts = {}
    node = cfg['nodes'][0]
    server = node['addr']
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    sftp = ssh.open_sftp()
    sftp.put('../resource_monitoring/monitor_linux.py', '/calvin-base/monitor_linux.py')

    print "Installing psutil needed by monitoring script"
    _, stdout, _ = ssh.exec_command("pip install psutil")
    stdout.channel.recv_exit_status()
    print "Finished installing psutil"

    for node in cfg['nodes']:
        server = node['addr']
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        _, stdout, _ = ssh.exec_command("pkill -f monitor_linux")
        stdout.channel.recv_exit_status() 

        cmd = "nohup python /calvin-base/monitor_linux.py > /tmp/monitor_linux.txt 2>&1 &"
        _, stdout, _ = ssh.exec_command(cmd)
        stdout.channel.recv_exit_status() 
        ssh.close()
