
Deploy Calvin in FIT IoT-LAB
============================

Requirements:
- Requests nodes either through web or experiment-cli interfaces
  - Minimum of 2 M3 nodes and 1 A8

Deploy base images:
- python setup_iotlab.py -i <job_id> -m <master_grid5000>
  - Clones and install calvin on A8 nodes
  - Create .iotlab file with username/password
  - Selects 1 M3 node to be the border node
  - Install image that reads temperature in the others
  - Install txthings and apply patch
  - setup_vpn.py -i <job_id>
    - Initializes the VPN to Grid5000 in each of A8 nodes
  - Run calvin runtimes
    - python runtimes_exec.py -c calvin_runtimes.json

  - Initialize tunslip:
    - nohup sudo  tunslip6.py -v2 -L -a m3-101 -p 20000 2001:660:5307:310B::/64 &
    - Test:
      - lynx -dump http://[2001:660:5307:310b::9181]
      - 9181 suffix: iotlab-experiment get -i 111272 -r, "uid" parameter of m3-101 node

