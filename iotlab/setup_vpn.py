import requests
import argparse
import paramiko
import os
import json
from os.path import expanduser

BASE_ADDR='https://www.iot-lab.info/rest'
IOTLAB_USER_FILE=os.path.expanduser("~/fitor/.iotlab")

def setup_vpn(servers):
    ssh_stdouts = {}
    for server in servers:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        print "Initializing VPN on node " + server
        _, stdout, _ = ssh.exec_command("pkill openvpn; sleep 1s; cd ~/A8/openresolv-3.9.0/; make install; cd ~/A8/grid5000_vpn/; nohup ./openvpn Grid5000_VPN.ovpn&")
        stdout.channel.recv_exit_status() 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Install calvin image in all A8 nodes of the experiment.')
    parser.add_argument('-i', '--id', type=int, help='Experiment ID', required=True)
    args = parser.parse_args()

    fit_username = ""
    fit_pwd = ""
    with open(IOTLAB_USER_FILE, 'r') as fit_file:
        fit_username = fit_file.readline().rstrip()
        fit_pwd = fit_file.readline().rstrip()

    addr = "/experiments/%d?resources" % args.id
    r = requests.get(BASE_ADDR + addr, auth=(fit_username, fit_pwd))
    setup_vpn(["node-" + server['network_address'] for server in r.json()['items'] if server['archi'] == "a8:at86rf231"])
