import json
import argparse
import paramiko

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Runs csruntimes in A8 nodes.')
    parser.add_argument('-c', '--config', type=str, help='Configuration file', required=True)
    parser.add_argument('-b', '--branch', type=str, help='Calvin version', required=False)
    parser.add_argument('-r', '--reconfig', type=str, help='Calvin reconfig algo', required=False)
    parser.add_argument('-l', '--learn', type=str, help='Calvin learn params', required=False)
    parser.add_argument('-d', '--deploy', type=str, help='Calvin deploy algo', required=False)
    parser.add_argument('-m', '--maintenance', type=int, help='Calvin maintenance interval', required=False)
    parser.add_argument('-co', '--cooldown', type=int, help='Calvin migration cooldown interval', required=False)
    args = parser.parse_args()

    with open(args.config) as json_data_file:
        cfg = json.load(json_data_file)

    # initial configs, setting reconfig and calvin version param
    node = cfg['nodes'][0]
    server = node['addr']
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    if args.branch is not None:
        print ('Setting calvin to version: %s' % args.branch)
	_, stdout, _ = ssh.exec_command("cd /calvin-base/; git reset --hard; git fetch; git checkout " + args.branch + ";")
        stdout.channel.recv_exit_status() 

    for node in cfg['nodes']:
        server = node['addr']
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        _, stdout, _ = ssh.exec_command("pkill csruntime")
        stdout.channel.recv_exit_status() 
    	if args.reconfig is not None:
    	    print ('Setting reconfig algo to: %s' % args.reconfig)
    	    _, stdout, _ = ssh.exec_command("cd /home/root/; sed -i -E 's/\"reconfig_algorithm\": \"[^\"]+\"/\"reconfig_algorithm\": \"%s\"/' .calvin.conf" % args.reconfig)
    	    stdout.channel.recv_exit_status() 
    	if args.deploy is not None:
    	    print ('Setting deploy algo to: %s' % args.deploy)
    	    _, stdout, _ = ssh.exec_command("cd /home/root/; sed -i -E 's/\"deployment_algorithm\": \"[^\"]+\"/\"deployment_algorithm\": \"%s\"/' .calvin.conf" % args.deploy)
    	    stdout.channel.recv_exit_status() 
    	if args.maintenance is not None:
    	    print ('Setting maintenance interval to: %d' % args.maintenance)
    	    _, stdout, _ = ssh.exec_command("cd /home/root/; sed -i -E 's/\"maintenance_delay\": [0-9]+/\"maintenance_delay\": %d/' .calvin.conf" % args.maintenance)
    	    stdout.channel.recv_exit_status() 
    	if args.cooldown is not None:
    	    print ('Setting migration cooldown interval to: %d' % args.cooldown)
    	    _, stdout, _ = ssh.exec_command("cd /home/root/; sed -i -E 's/\"migration_cooldown\": [0-9]+/\"migration_cooldown\": %d/' .calvin.conf" % args.cooldown)
    	    stdout.channel.recv_exit_status() 
    	if args.learn is not None:
    	    print ('Setting learn params to: %s' % args.learn)
    	    _, stdout, _ = ssh.exec_command("cd /home/root/; sed -i -E 's/\"learn\": \{.*\}/\"learn\": \{%s\}/' .calvin.conf" % args.learn)
    	    stdout.channel.recv_exit_status() 

        _, stdout, _ = ssh.exec_command("/sbin/ifconfig tun0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'")
        ip_addr = stdout.read()
        if (ip_addr == '' or ip_addr == None or ip_addr.isspace()):
            print("Re-initializing VPN on %s" % server)
            _, stdout, _ = ssh.exec_command("dhclient eth0")
            stdout.channel.recv_exit_status()
            _, stdout, _ = ssh.exec_command("pkill openvpn; sleep 1s; cd ~/A8/openresolv-3.9.0/; make install; cd ~/A8/grid5000_vpn/; nohup ./openvpn Grid5000_VPN.ovpn&")
            stdout.channel.recv_exit_status()
        _, stdout, _ = ssh.exec_command("ps cax | grep netdata")
        netdata_output = stdout.channel.recv_exit_status()
        if (netdata_output == 1):
            print("Re-initializing NETDATA on %s" % server)
            _, stdout, _ = ssh.exec_command("dhclient eth0")
            cmd = "opkg install ~/A8/base-passwd_3.5.29-r0_cortexa8hf-neon.ipk; opkg install ~/A8/update-alternatives-opkg_0.1.8+git0+53274f0875-r0_cortexa8hf-    neon.ipk; opkg install ~/A8/shadow_4.2.1-r0_cortexa8hf-neon.ipk; opkg install --force-reinstall ~/A8/netdata_1.8.0+git0+89ed309252-r0_cortexa8hf-neon.ipk"
            _, stdout, _ = ssh.exec_command(cmd)
            stdout.channel.recv_exit_status()
            _, stdout, _ = ssh.exec_command("netdata")
            stdout.channel.recv_exit_status()


    index = 0
    for node in cfg['nodes']:
        server = node['addr']
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        _, stdout, _ = ssh.exec_command("/sbin/ifconfig tun0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'")
        ip_addr = stdout.read()
        name = "endpoint%d" % index
        index += 1
        if "uuid" in node:
            cmd = "CALVIN_GLOBAL_STORAGE_TYPE='\"proxy\"' CALVIN_GLOBAL_STORAGE_PROXY='\"calvinip://" + cfg['server_addr'] + "\"' nohup csruntime -n " + ip_addr.split('\n')[0] + " --gui --name \"" + name + "\" --uuid \"" + node["uuid"] +  "\" --loglevel=INFO --logfile=/tmp/calvin.log --attr '" + node['attr'] + "'  >> /tmp/calvin.out 2>> /tmp/calvin.out &"
        else:
            cmd = "CALVIN_GLOBAL_STORAGE_TYPE='\"proxy\"' CALVIN_GLOBAL_STORAGE_PROXY='\"calvinip://" + cfg['server_addr'] + "\"' nohup csruntime -n " + ip_addr.split('\n')[0] + " --gui --name \"" + name + "\" --loglevel=INFO --logfile=/tmp/calvin.log --attr '" + node['attr'] + "' >> /tmp/calvin.out 2>> /tmp/calvin.out &"
        print "Initializing csruntime on node " + server
        print "Remote command: " + cmd
        _, stdout, _ = ssh.exec_command(cmd)
        stdout.channel.recv_exit_status() 
        cmd = "pkill -f mtail"
        print "Remote command: " + cmd
        _, stdout, _ = ssh.exec_command(cmd)
        stdout.channel.recv_exit_status()
        cmd = "nohup ~/A8/mtail_v3.0.0-rc12_linux_arm -progs ~/A8/ -logs /tmp/calvin.log > /tmp/mtail_output.txt 2>&1 &"
        print "Remote command: " + cmd
        _, stdout, _ = ssh.exec_command(cmd)
        stdout.channel.recv_exit_status()
        ssh.close()
        print "END"
