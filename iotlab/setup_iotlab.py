import requests
import argparse
import paramiko
import json
import os
import subprocess
from os.path import expanduser

IOTLAB_USER_FILE=os.path.expanduser("~/fitor/.iotlab")
# FIXME: Adjust to point to your copy of A8 image
A8_IMAGE='bdemouradonassolo@fsophia.sophia.grid5000.fr:~/image/image/'

BASE_ADDR='https://www.iot-lab.info/rest'
BRANCH_BASE='calvinDevelop122018'
#BRANCH_BASE='developAndNetworkAndPaper'
#BRANCH_BASE='ccncDemo'
BORDER_IMAGE=expanduser("~")+"/A8/border-router-ch4.iotlab-m3"
COAP_IMAGE=expanduser("~")+"/A8/er-example-server-ch4-temp.iotlab-m3"

TXTHINGS_PATCH='''
--- coap_orig.py        2017-11-09 11:04:46.500000000 +0100
+++ coap.py     2017-11-09 11:05:05.012000000 +0100
@@ -775,7 +775,7 @@

     def datagramReceived(self, data, (host, port)):
         log.msg("Received %r from %s:%d" % (data, host, port))
-        message = Message.decode(data, (ip_address(host), port), self)
+        message = Message.decode(data, (ip_address(unicode(host)), port), self)
         if self.deduplicateMessage(message) is True:
             return
         if isRequest(message.code):
'''

YACC_PATCH='''
--- /usr/lib/python2.7/site-packages/ply/yacc.py        2018-12-06 16:11:39.692000000 +0100
+++ /tmp/orig.py        2018-12-06 16:11:32.489889701 +0100
@@ -111,8 +111,7 @@
         self.f = f
 
     def debug(self, msg, *args, **kwargs):
-        self.f.write((msg % args) + '\n')
+        pass
+        #self.f.write((msg % args) + '\n')
 
     info = debug
'''

CALVIN_CONF='''
{
  "global" : {
    "deployment_algorithm": "money",
    "grasp": "v2",
    "deployment_n_samples": 50,
    "maintenance_delay": 5,
    "migration_cooldown": 10,
    "reconfig_algorithm": "app_v0"
  },
  "calvinsys": {
     "capabilities": {
         "io.stdout": {
           "module": "ui.StandardOut",
           "attributes": {"ui_def": {"control":{"type":"console"}}}
         }
      }
   },
  "learn": { }
}
'''

def upgrade_pip(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Upgrading pip " + server
    _, stdout, _ = ssh.exec_command("pip install --upgrade pip")
    stdout.channel.recv_exit_status()
    print "Finished pip upgrade"

def install_txthing(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Install txthings and apply patch " + server
    sftp = ssh.open_sftp()
    import StringIO
    sftp.putfo(StringIO.StringIO(TXTHINGS_PATCH), "/tmp/txthings.patch")
    _, stdout, _ = ssh.exec_command("pip install txthings==0.2; patch /usr/lib/python2.7/site-packages/txthings/coap.py /tmp/txthings.patch")
    stdout.channel.recv_exit_status()
    print "Finished install of txthing"

def fix_yacc(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    sftp = ssh.open_sftp()
    import StringIO
    sftp.putfo(StringIO.StringIO(YACC_PATCH), "/tmp/yacc.patch")
    _, stdout, _ = ssh.exec_command("patch /usr/lib/python2.7/site-packages/ply/yacc.py /tmp/yacc.patch")
    stdout.channel.recv_exit_status()
    print "Finished yacc fix"

def install_rpcudp(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Install rpcudp in right version, 1.0: " + server
    _, stdout, _ = ssh.exec_command("pip install rpcudp==1.0")
    stdout.channel.recv_exit_status()
    print "Finished install of rpcudp"

def install_pyasn1(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Upgrade pyasn1 version: " + server
    _, stdout, _ = ssh.exec_command("pip install --upgrade pyasn1")
    stdout.channel.recv_exit_status()
    print "Finished install of pyasn1"

def rsync_image(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Rsync image on node " + server
    _, stdout, _ = ssh.exec_command("rsync  -e \"ssh -o StrictHostKeyChecking=no\" -qavxHAX " + A8_IMAGE + " /")
    stdout.channel.recv_exit_status()
    print "Finished rsync on node"

def install_calvin(servers):
    server = servers[0]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username="root")
    print "Started install of calvin on node " + server
    _, stdout, _ = ssh.exec_command("git clone https://github.com/brunodonassolo/calvin-base.git /calvin-base/; cd /calvin-base/; git reset --hard; git checkout " + BRANCH_BASE + "; git pull; pip install --extra-index-url=https://www.piwheels.org/simple -e .")
    stdout.channel.recv_exit_status()
    sftp = ssh.open_sftp()
    import StringIO
    sftp.putfo(StringIO.StringIO(CALVIN_CONF), "/home/root/.calvin.conf")
    print "Finished install of calvin on node"

def setup_border_router(id, nodes, user, password):
    print "Installing border node " + str(nodes)
    addr = "/experiments/%d/nodes?update" % id
    with open(BORDER_IMAGE, 'rb') as f: r = requests.post(BASE_ADDR + addr, auth=(user, password), files={BORDER_IMAGE: f, 'nodes.json': json.dumps(nodes)})
    print r.json()

    print "Running tunslip"
    os.system("nohup sudo tunslip6.py -v2 -L -a %s -p 20000 2001:660:5307:310B::/64 &" % nodes[0])


def setup_coap_nodes(id, nodes, user, password):
    print "Installing CoAP image on nodes: " + str(nodes)
    addr = "/experiments/%d/nodes?update" % id
    with open(COAP_IMAGE, 'rb') as f: r = requests.post(BASE_ADDR + addr, auth=(user, password), json=nodes, files={COAP_IMAGE: f, 'nodes.json': json.dumps(nodes)})
    print r.json()

def run_blackbox(servers):
    for server in servers:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        print "Start blackbox_exporter on " + server
        _, stdout, _ = ssh.exec_command("killall blackbox_exporter; cd ~/A8/blackbox_exporter-0.10.0.linux-armv7/; ./blackbox_exporter > /dev/null 2>&1 &")
        stdout.channel.recv_exit_status()
        ssh.close()

def run_runtimes(master_addr, a8_servers, cpuA8):
    with open('calvin_runtimes_generated.json', 'w+') as fp:
        fp.write("{\n")
        fp.write("\t\"server_addr\" : \"%s\",\n" % master_addr)
        fp.write("\t\"nodes\" : [\n")
        for server in a8_servers:
            if server != a8_servers[0]:
                fp.write(",\n")
            else:
                fp.write("\n")

            fp.write("\t\t{\n")
            fp.write("\t\t\"addr\" : \"%s\",\n" % server)
            fp.write("\t\t\"attr\" :  \"{\\\"indexed_public\\\": {\\\"node_name\\\": {\\\"group\\\": \\\"endpoint\\\"}, \\\"cpuTotal\\\": \\\"%d\\\", \\\"memTotal\\\": \\\"100M\\\" }, \\\"public\\\": {\\\"cost_cpu\\\": 0.9, \\\"cost_ram\\\": 0.9}}\"\n" % cpuA8)
            fp.write("\t\t}")
        fp.write("\n\t]\n")
        fp.write("}\n")
        fp.close()
    print "Installing monitoring script in A8"
    os.system("python runtimes_install_monitoring.py -c calvin_runtimes_generated.json")

    print "Starting calvin's runtimes"
    os.system("python runtimes_exec.py -c calvin_runtimes_generated.json")

def setup_netdata(servers):
    for server in servers:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        print "Started netdata install on node " + server
        cmd = "opkg install ~/A8/base-passwd_3.5.29-r0_cortexa8hf-neon.ipk; opkg install ~/A8/update-alternatives-opkg_0.1.8+git0+53274f0875-r0_cortexa8hf-neon.ipk; opkg install ~/A8/shadow_4.2.1-r0_cortexa8hf-neon.ipk; opkg install --force-reinstall ~/A8/netdata_1.8.0+git0+89ed309252-r0_cortexa8hf-neon.ipk"
        _, stdout, _ = ssh.exec_command(cmd)
        code = stdout.channel.recv_exit_status()
        print "Finished install netdata on node %s, code: %d" % (server, code)

    ssh_stdouts = {}
    for server in servers:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, username="root")
        _, stdout, _ = ssh.exec_command("killall netdata; netdata")
        ssh_stdouts[server] = stdout

    for key, stdout in ssh_stdouts.iteritems():
        code = stdout.channel.recv_exit_status()
        print "Running netdata on node %s, code: %d" % (key, code)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Install calvin image in all A8 nodes of the experiment.')
    parser.add_argument('-i', '--id', type=int, help='Experiment ID', required=True)
    parser.add_argument('-m', '--master', type=str, help='Address of master node in grid5000 (e.g. genepi-18.grenoble.grid5000.fr:5004', required=True)
    parser.add_argument('--cpuA8', type=int, help="A8 CPU Total power", default=1000)
    args = parser.parse_args()

    fit_username = ""
    fit_pwd = ""
    with open(IOTLAB_USER_FILE, 'r') as fit_file:
        fit_username = fit_file.readline().rstrip()
        fit_pwd = fit_file.readline().rstrip()

    addr = "/experiments/%d?resources" % args.id
    r = requests.get(BASE_ADDR + addr, auth=(fit_username, fit_pwd))
    a8_nodes = ["node-" + server['network_address'] for server in r.json()['items'] if server['archi'] == "a8:at86rf231"]
    m3_nodes = [server['network_address'] for server in r.json()['items'] if server['archi'] == "m3:at86rf231"]
    if len(a8_nodes) < 1:
        print "Insufficient number of A8 nodes: %d. Mininum 1" % len(a8_nodes)
        exit()

    if len(m3_nodes) < 2:
        print "Insufficient number of M3 nodes: %d. Mininum 2" % len(m3_nodes)
        exit()

    os.system("python ./setup_vpn.py -i %s" % (args.id))
    rsync_image(a8_nodes)
    upgrade_pip(a8_nodes)
    install_rpcudp(a8_nodes)
    install_txthing(a8_nodes)
    install_pyasn1(a8_nodes)
    print "Border router"
    setup_border_router(args.id, m3_nodes[:1], fit_username, fit_pwd)
    print "CoAP nodes"
    setup_coap_nodes(args.id, m3_nodes[1:], fit_username, fit_pwd)
    print "Installing calvin"
    install_calvin(a8_nodes)
    fix_yacc(a8_nodes)

    print "Blackbox"
    run_blackbox(a8_nodes)

    print "Netdata"
    setup_netdata(a8_nodes)


    run_runtimes(args.master, a8_nodes, args.cpuA8)
    print "iotlab setup script finished"
